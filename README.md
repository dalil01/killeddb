# KilledDB

**09/02/2022 -> 09/06/2022**

## Cahier des charges de la mission
- <a href="docs/specifications.pdf">docs/specifications.pdf</a>

## Exécuter l'application
```sh
mvn jetty:run
mvn jetty:run -Djetty.http.port=8081
```