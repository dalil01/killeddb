package com.alphasquad.killeddb.endpoints;

import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.QueryException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import com.alphasquad.killeddb.services.CSVService;
import com.alphasquad.killeddb.services.TaskExecutorService;
import org.jboss.resteasy.annotations.GZIP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Path("node")
@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.MULTIPART_FORM_DATA})
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
@GZIP
public class NodeEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeEndpoint.class);

    private final CSVService csvService = new CSVService();
    private final TaskExecutorService taskExecutorService = new TaskExecutorService();

    @POST
    @Path("/ping")
    public Response ping(@Context UriInfo uriInfo) {
        return Response.status(Response.Status.OK).location(uriInfo.getAbsolutePath()).build();
    }

    @POST
    @Path("/execute-task")
    public Response executeTask(Task task) throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException, ClassNotFoundException {
        Response.Status status = Response.Status.BAD_REQUEST;

        LOGGER.info(task.getTaskName().toString());

        Object response = this.taskExecutorService.execute(task);
        if (response != null) {
            status = Response.Status.OK;
        }

        return Response.status(status).entity(response).build();
    }

    @POST
    @Path("/insert-lines/{schemaName}/{tableName}/{delimiter}/{columnsByIndex}")
    public Response insertLines(@PathParam("schemaName") String schemaName, @PathParam("tableName") String tableName, @PathParam("delimiter") String delimiter, @PathParam("columnsByIndex") String columnsByIndex, List<String> lines) throws SchemaException, TableException {
        Response.Status status = Response.Status.BAD_REQUEST;

        LOGGER.info(TaskNames.INSERT_DATA_FROM_CSV.toString());

        Object response = this.csvService.load(schemaName, tableName, delimiter, columnsByIndex, lines);
        if (response != null) {
            status = Response.Status.OK;
        }

        return Response.status(status).entity(response).build();
    }

}
