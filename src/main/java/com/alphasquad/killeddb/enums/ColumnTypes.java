package com.alphasquad.killeddb.enums;

public enum ColumnTypes {
    BOOLEAN,
    BYTE,
    CHARACTER,
    DATETIME,
    DOUBLE,
    FLOAT,
    INTEGER,
    LONG,
    SHORT,
    STRING
}
