package com.alphasquad.killeddb.enums;

import java.util.EnumSet;

public enum TaskNames {
    CREATE_SCHEMA,
    RENAME_SCHEMA,
    DROP_SCHEMA,

    CREATE_TABLE,
    RENAME_TABLE,
    DROP_TABLE,

    ADD_COLUMNS,
    CHANGE_COLUMNS,
    DROP_COLUMNS,

    INSERT_DATA,
    INSERT_DATA_FROM_CSV,
    SELECT_DATA,
    UPDATE_DATA,
    UPDATE_DATA_FROM_CSV,
    DELETE_DATA;

    public static final EnumSet<TaskNames> POST_TASKS = EnumSet.of(CREATE_SCHEMA, CREATE_TABLE, ADD_COLUMNS, INSERT_DATA);
    public static final EnumSet<TaskNames> GET_TASKS = EnumSet.of(SELECT_DATA);
    public static final EnumSet<TaskNames> PUT_TASKS = EnumSet.of(RENAME_SCHEMA, RENAME_TABLE, CHANGE_COLUMNS, UPDATE_DATA);
    public static final EnumSet<TaskNames> DELETE_TASKS = EnumSet.of(DROP_SCHEMA, DROP_TABLE, DROP_COLUMNS, DELETE_DATA);
}

