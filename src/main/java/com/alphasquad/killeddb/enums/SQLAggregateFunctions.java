package com.alphasquad.killeddb.enums;

public enum SQLAggregateFunctions {
    COUNT,
    COUNT_ALL,
    COUNT_DISTINCT,
    MIN,
    MAX,
    SUM,
    AVG
}
