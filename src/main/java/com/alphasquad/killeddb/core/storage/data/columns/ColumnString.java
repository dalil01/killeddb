package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;

import java.util.*;

public class ColumnString extends Column<String> {

    public static String DEFAULT_VALUE = "";

    public ColumnString(String name, ColumnTypes type) {
        super(name, type);
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(value);
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, newValue);
    }

    @Override
    public String getParsedValue(String value) {
        return value;
    }

    @Override
    public String getValueAsColumnType(Object value) {
        return (String) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> !v.equals(ColumnString.DEFAULT_VALUE))
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> (String) m.get(alias))
                .filter(v -> !v.equals(ColumnString.DEFAULT_VALUE))
                .count();
    }

    @Override
    public String getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public String getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (String) m.get(alias))
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public String getMin(Object currentMin, String nodeMin) {
        String currentMinCast = this.getValueAsColumnType(currentMin);
        return nodeMin.compareTo(currentMinCast) < 0 ? nodeMin : currentMinCast;
    }

    @Override
    public String getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public String getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (String) m.get(alias))
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public String getMax(Object currentMax, String nodeMax) {
        String currentMaxCast = this.getValueAsColumnType(currentMax);
        return nodeMax.compareTo(currentMaxCast) > 0 ? nodeMax : currentMaxCast;
    }

    @Override
    public Number getSum() {
        return null;
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public Double getAvg() {
        return null;
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && value.equals(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && ((String)value).compareTo(compValue) < 0;
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && ((String)value).compareTo(compValue) > 0;
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
