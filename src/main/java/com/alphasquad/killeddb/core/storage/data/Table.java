package com.alphasquad.killeddb.core.storage.data;

import com.alphasquad.killeddb.core.storage.data.columns.Column;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Table {

    @EqualsAndHashCode.Include
    private String name;
    private final Map<String, Column<Class<?>>> columns = new LinkedHashMap<>();
    private long size = 0;

    public Table(String name) {
        this.name = name;
    }

    public void addColumn(Column<Class<?>> column) {
        this.columns.put(column.getName(), column);
    }

    public void incrementSize() {
        this.size++;
    }

    public void decrementSize() {
        this.size--;
    }

    public void setSizeAuto() {
        this.size = this.columns.isEmpty() ? this.size : this.columns.values().iterator().next().getValues().size();
    }

}
