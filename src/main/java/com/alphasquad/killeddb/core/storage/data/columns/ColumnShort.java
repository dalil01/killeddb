package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;

import java.util.*;

public class ColumnShort extends Column<Short> {

    public static short DEFAULT_VALUE = 0;

    public ColumnShort(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new ShortArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseShort(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseShort(newValue));
    }

    @Override
    public Short getParsedValue(String value) {
        return Parser.parseShort(value);
    }

    @Override
    public Short getValueAsColumnType(Object value) {
        return (Short) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnShort.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).shortValue())
                .filter(v -> v != ColumnShort.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Short getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Short getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).shortValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Short getMin(Object currentMin, String nodeMin) {
        Short currentMinCast = this.getValueAsColumnType(currentMin);
        Short parsedNodeMin = Parser.parseShort(nodeMin);
        return currentMinCast < parsedNodeMin ? currentMinCast : parsedNodeMin;
    }

    @Override
    public Short getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Short getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).shortValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Short getMax(Object currentMax, String nodeMax) {
        Short currentMaxCast = this.getValueAsColumnType(currentMax);
        Short parsedNodeMax = Parser.parseShort(nodeMax);
        return currentMaxCast > parsedNodeMax ? currentMaxCast : parsedNodeMax;
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .mapToInt(Short::intValue)
                .sum();
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).shortValue())
                .mapToDouble(Short::shortValue)
                .sum();
    }

    @Override
    public Integer getSum(Object currentMin, String nodeMin) {
        return this.getValueAsColumnType(currentMin) + Parser.parseShort(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToDouble(Short::shortValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).shortValue())
                .mapToDouble(Short::shortValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return ((Double) currentMin + Parser.parseShort(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (short)value == Parser.parseShort(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (short)value < Parser.parseShort(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (short)value > Parser.parseShort(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
