package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class Column<T> {

    @EqualsAndHashCode.Include
    private String name;
    private ColumnTypes type;
    private List<T> values = new ArrayList<>();

    public Column(String name, ColumnTypes type) {
        this.name = name;
        this.type = type;
    }

    public abstract void addValue(String value);

    public abstract void setValue(int index, String newValue);

    public T getValue(int index) {
        return this.values.get(index);
    }

    public abstract T getParsedValue(String value);

    public abstract T getValueAsColumnType(Object value);

    public synchronized void addValues(Collection<?> parsedValues) {
        this.values.addAll((Collection<? extends T>) parsedValues);
    }

    public void clearValues() {
        this.values.clear();
    }

    public abstract long getCount();

    public abstract long getCount(List<Map<String, Object>> rows, String alias);

    public long getCount(long currentCount, String nodeCount) {
        return currentCount + Parser.parseLong(nodeCount);
    }

    public long getCountDistinct() {
        return this.values
                .parallelStream()
                .distinct()
                .count();
    }

    public long getCountDistinct(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> m.get(alias))
                .distinct()
                .count();
    }

    public long getCountDistinct(long currentCount, String nodeCount) {
        return Math.max(currentCount, Parser.parseLong(nodeCount));
    }

    public abstract T getMin();

    public abstract T getMin(List<Map<String, Object>> rows, String alias);

    public abstract T getMin(Object currentMin, String nodeMin);

    public abstract T getMax();

    public abstract T getMax(List<Map<String, Object>> rows, String alias);

    public abstract T getMax(Object currentMax, String nodeMax);

    public abstract Number getSum();

    public abstract Number getSum(List<Map<String, Object>> rows, String alias);

    public abstract Number getSum(Object currentMin, String nodeMin);

    public abstract Double getAvg();

    public abstract Double getAvg(List<Map<String, Object>> rows, String alias);

    public abstract Double getAvg(Object currentMin, String nodeMin);

    public abstract boolean equalValues(Object value, String compValue);

    public abstract boolean lowerValues(Object value, String compValue);

    public abstract boolean higherValues(Object value, String compValue);

    public abstract boolean lowerOrEqualValues(Object value, String compValue);

    public abstract boolean higherOrEqualValues(Object value, String compValue);

    public boolean startWithValues(Object value, String compValue) {
        return String.valueOf(value).toLowerCase().startsWith(compValue);
    }

    public boolean endWithValues(Object value, String compValue) {
        return String.valueOf(value).toLowerCase().endsWith(compValue);
    }

    public boolean containValues(Object value, String compValue) {
        return String.valueOf(value).toLowerCase().contains(compValue);
    }

    public boolean isNumber() {
        return this.type == ColumnTypes.BYTE
                || this.type == ColumnTypes.DOUBLE
                || this.type == ColumnTypes.FLOAT
                || this.type == ColumnTypes.INTEGER
                || this.type == ColumnTypes.LONG
                || this.type == ColumnTypes.SHORT;
    }

    public boolean isString() {
        return this.type == ColumnTypes.STRING;
    }

    public boolean isChar() {
        return this.type == ColumnTypes.CHARACTER;
    }

    public boolean isDate() {
        return this.type == ColumnTypes.DATETIME;
    }

}
