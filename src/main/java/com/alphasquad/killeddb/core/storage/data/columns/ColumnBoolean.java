package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.booleans.BooleanArrayList;

import java.util.*;

public class ColumnBoolean extends Column<Boolean> {

    public ColumnBoolean(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new BooleanArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseBoolean(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseBoolean(newValue));
    }

    @Override
    public Boolean getParsedValue(String value) {
        return Parser.parseBoolean(value);
    }

    @Override
    public Boolean getValueAsColumnType(Object value) {
        return (Boolean) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> (boolean) m.get(alias))
                .filter(v -> v)
                .count();
    }

    @Override
    public Boolean getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Boolean getMin(List<Map<String, Object>> rows, String alias) {
        return rows.
                parallelStream()
                .map(m -> (Boolean) m.get(alias))
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Boolean getMin(Object currentMin, String nodeMin) {
        boolean parsedNodeMin = Parser.parseBoolean(nodeMin);
        return parsedNodeMin && this.getValueAsColumnType(currentMin);
    }

    @Override
    public Boolean getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Boolean getMax(List<Map<String, Object>> rows, String alias) {
        return rows.
                parallelStream()
                .map(m -> (Boolean) m.get(alias))
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Boolean getMax(Object currentMax, String nodeMax) {
        boolean parsedNodeMax = Parser.parseBoolean(nodeMax);
        return parsedNodeMax || this.getValueAsColumnType(currentMax);
    }

    @Override
    public Number getSum() {
        return null;
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public Double getAvg() {
        return null;
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (boolean)value == Parser.parseBoolean(compValue) ;
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return false;
    }

}
