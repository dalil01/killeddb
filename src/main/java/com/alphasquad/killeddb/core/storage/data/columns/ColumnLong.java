package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.longs.LongArrayList;

import java.util.*;

public class ColumnLong extends Column<Long> {

    public static long DEFAULT_VALUE = 0;

    public ColumnLong(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new LongArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseLong(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseLong(newValue));
    }

    @Override
    public Long getParsedValue(String value) {
        return Parser.parseLong(value);
    }

    @Override
    public Long getValueAsColumnType(Object value) {
        return (Long) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnLong.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).longValue())
                .filter(v -> v != ColumnLong.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Long getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).longValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMin(Object currentMin, String nodeMin) {
        Long currentMinCast = this.getValueAsColumnType(currentMin);
        return Math.min(currentMinCast, Parser.parseLong(nodeMin));
    }

    @Override
    public Long getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).longValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMax(Object currentMax, String nodeMax) {
        Long currentMaxCast = this.getValueAsColumnType(currentMax);
        return Math.max(currentMaxCast, Parser.parseLong(nodeMax));
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .reduce(0L, Long::sum);
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).longValue())
                .reduce(0L, Long::sum);
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return this.getValueAsColumnType(currentMin) + Parser.parseLong(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToLong(Long::longValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).longValue())
                .mapToLong(Long::longValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return ((Double) currentMin + Parser.parseDouble(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (long)value == Parser.parseLong(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (long)value < Parser.parseLong(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (long)value > Parser.parseLong(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
