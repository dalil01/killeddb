package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.floats.FloatArrayList;

import java.util.*;

public class ColumnFloat extends Column<Float> {

    public static float DEFAULT_VALUE = 0;

    public ColumnFloat(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new FloatArrayList());

    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseFloat(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseFloat(newValue));
    }

    @Override
    public Float getParsedValue(String value) {
        return Parser.parseFloat(value);
    }

    @Override
    public Float getValueAsColumnType(Object value) {
        return (Float) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnFloat.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> ((Number) m.get(alias)).floatValue())
                .filter(v -> v != ColumnFloat.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Float getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Float getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).floatValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Float getMin(Object currentMin, String nodeMin) {
        Float currentMinCast = this.getValueAsColumnType(currentMin);
        return Math.min(currentMinCast, Parser.parseFloat(nodeMin));
    }

    @Override
    public Float getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Float getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).floatValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Float getMax(Object currentMax, String nodeMax) {
        Float currentMaxCast = this.getValueAsColumnType(currentMax);
        return Math.max(currentMaxCast, Parser.parseFloat(nodeMax));
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .reduce(0F, Float::sum);
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).floatValue())
                .mapToDouble(Float::floatValue)
                .sum();
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return this.getValueAsColumnType(currentMin) + Parser.parseFloat(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToDouble(Float::floatValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).floatValue())
                .mapToInt(Float::byteValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return ((Double) currentMin + Parser.parseDouble(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (float)value == Parser.parseFloat(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (float)value < Parser.parseFloat(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (float)value > Parser.parseFloat(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
