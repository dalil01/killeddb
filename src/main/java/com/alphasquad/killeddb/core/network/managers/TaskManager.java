package com.alphasquad.killeddb.core.network.managers;

import com.alphasquad.killeddb.core.network.Node;
import com.alphasquad.killeddb.core.network.Task;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class TaskManager {

    private TaskManager() {
    }

    public static TaskManager get() {
        return TaskManager.Instance.INSTANCE;
    }

    public boolean canDistribute() {
        return NodeManager.get().getActiveNodes().size() > 1;
    }

    public void distributeTaskToNextNode(Task task) {
        NodeManager.get().getNextNode().executeTaskSync(task);
    }

    public void distributeSyncTaskToOtherNodes(Task task) {
        for (Node node : NodeManager.get().getActiveNodes()) {
            if (!node.isCurrent())
                node.executeTaskAsync(task);
        }
    }

    public List<Future<Response>> distributeAsyncTaskToOtherNodes(Task task) {
        List<Future<Response>> responses = new ArrayList<>();

        for (Node node : NodeManager.get().getActiveNodes()) {
            if (!node.isCurrent())
                responses.add(node.executeTaskAsync(task));
        }

        return responses;
    }

    private static class Instance {
        private static final TaskManager INSTANCE = new TaskManager();
    }

}
