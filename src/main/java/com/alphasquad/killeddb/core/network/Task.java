package com.alphasquad.killeddb.core.network;

import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.HashMap;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Task {

    @EqualsAndHashCode.Include
    private TaskNames taskName;
    private Map<TaskDataMapKeys, Object> dataMap = new HashMap<>();

    public Task() {
    }

    public Task(TaskNames taskName) {
        this.taskName = taskName;
    }

    public Task(TaskNames taskName, Map<TaskDataMapKeys, Object> dataMap) {
        this.taskName = taskName;
        this.dataMap = dataMap;
    }

    public void addData(TaskDataMapKeys key, Object value) {
        this.dataMap.put(key, value);
    }

}
