package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.enums.SQLAggregateFunctions;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.QueryException;
import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Documentation SQL : https://sql.sh/
 */
public class SQLQueryService {

    public Task getTask(String schemaName, String query) throws QueryException, SchemaException, TableException {
        if (query == null || StringUtils.countMatches(query, ";") > 1) {
            throw new QueryException();
        }

        String trimQuery = query.replace(";", "").toLowerCase().trim();
        List<String> splitQueries = List.of(trimQuery.split(" "));

        if (splitQueries.size() < 3) {
            throw new QueryException();
        }

        Task task = new Task();

        String[] schemaQueryMatches = {"create", "alter", "drop"};
        String[] tableQueryMatches = {"create", "alter", "drop", "truncate"};
        String[] tableDataQueryMatches = {"insert into", "select", "update", "delete"};

        boolean queryStartWithGoodMatch = this.queryStartWithGoodMatch(trimQuery, schemaQueryMatches, " schema ");
        if (queryStartWithGoodMatch) {
            this.setSchemaTask(trimQuery, task);
        } else {
            queryStartWithGoodMatch = this.queryStartWithGoodMatch(trimQuery, tableQueryMatches, " table ");

            if (queryStartWithGoodMatch) {
                this.setTableTask(trimQuery, task);
            } else {
                queryStartWithGoodMatch = this.queryStartWithGoodMatch(trimQuery, tableDataQueryMatches, " ");

                if (queryStartWithGoodMatch)
                    this.setTableDataTask(schemaName, trimQuery, task);
            }
        }

        if (!queryStartWithGoodMatch || task.getTaskName() == null) {
            throw new QueryException();
        }

        return task;
    }

    public static ColumnTypes getParsedType(String type) throws QueryException {
        switch (type.toLowerCase(Locale.ROOT)) {
            case "boolean", "tinyint" -> {
                return ColumnTypes.BOOLEAN;
            }
            case "byte" -> {
                return ColumnTypes.BYTE;
            }
            case "char" -> {
                return ColumnTypes.CHARACTER;
            }
            case "datetime", "date", "timestamp" -> {
                return ColumnTypes.DATETIME;
            }
            case "decimal", "double" -> {
                return ColumnTypes.DOUBLE;
            }
            case "float" -> {
                return ColumnTypes.FLOAT;
            }
            case "int", "integer" -> {
                return ColumnTypes.INTEGER;
            }
            case "long", "bigint" -> {
                return ColumnTypes.LONG;
            }
            case "smallint" -> {
                return ColumnTypes.SHORT;
            }
            case "string", "varchar", "text" -> {
                return ColumnTypes.STRING;
            }
            default -> throw new QueryException(type + " is invalid type.");
        }
    }

    public static SQLAggregateFunctions getSQLAggregateFunction(String agg) throws QueryException {
        agg = agg.toLowerCase(Locale.ROOT).toLowerCase(Locale.ROOT).replaceAll(" ", "");
        switch (StringUtils.substringBefore(agg, "(").trim()) {
            case "count" -> {
                String countParam = StringUtils.substringBetween(agg, "(", ")");
                if (countParam.equals("*")) {
                    return SQLAggregateFunctions.COUNT_ALL;
                } else if (countParam.contains("distinct")) {
                    return SQLAggregateFunctions.COUNT_DISTINCT;
                }
                return SQLAggregateFunctions.COUNT;
            }

            case "min" -> {
                return SQLAggregateFunctions.MIN;
            }

            case "max" -> {
                return SQLAggregateFunctions.MAX;
            }

            case "sum" -> {
                return SQLAggregateFunctions.SUM;
            }

            case "avg" -> {
                return SQLAggregateFunctions.AVG;
            }

            default -> throw new QueryException(agg + " is invalid aggregation.");
        }
    }

    private boolean queryStartWithGoodMatch(String query, String[] matches, String moreText) {
        for (String m : matches) {
            if (query.startsWith(m + ((moreText != null) ? moreText : "")))
                return true;
        }

        return false;
    }

    /**
     * Ex :
     * -> CREATE SCHEMA schemaName; ✔️
     * -> ALTER SCHEMA schemaName RENAME newSchemaName; ✔️
     * -> DROP SCHEMA schemaName; ✔️
     */
    private void setSchemaTask(String trimQuery, Task task) throws QueryException {
        List<String> splitQuery = List.of(trimQuery.split(" "));

        if (splitQuery.size() != 3 && splitQuery.size() != 5) {
            throw new QueryException();
        }

        switch (splitQuery.get(0).trim()) {
            case "create" -> task.setTaskName(TaskNames.CREATE_SCHEMA);
            case "alter" -> {
                if (splitQuery.get(3).trim().equals("rename")) {
                    task.setTaskName(TaskNames.RENAME_SCHEMA);
                    task.addData(TaskDataMapKeys.NEW_SCHEMA_NAME, splitQuery.get(4).trim());
                } else {
                    throw new QueryException();
                }
            }
            case "drop" -> task.setTaskName(TaskNames.DROP_SCHEMA);
        }

        task.addData(TaskDataMapKeys.SCHEMA_NAME, splitQuery.get(2).trim());
    }

    /**
     * Ex :
     * -> CREATE TABLE tableName (columnName columnType, columnName columnType, columnName columnType...); ✔️
     * -> ALTER TABLE tableName RENAME TO newTableName; ✔️
     * -> ALTER TABLE tableName ADD COLUMN columnName columnType; ✔️
     * -> ALTER TABLE tableName ADD COLUMN columnName1 columnType1, ADD COLUMN columnName2 columnType2, ADD COLUMN columnName3 columnType3, ...; ✔️
     * -> ALTER TABLE tableName CHANGE columnName newColumnName newColumnType; ✔️
     * -> ALTER TABLE tableName CHANGE columnName newColumnName newColumnType, CHANGE columnName2 newColumnName2 newColumnType2, ...; ✔️
     * -> ALTER TABLE tableName DROP COLUMN columnName1; ✔️
     * -> ALTER TABLE tableName DROP COLUMN columnName1, DROP COLUMN columnName2, ...; ✔️
     * -> DROP TABLE tableName; ✔️
     */
    private void setTableTask(String trimQuery, Task task) throws QueryException {
        List<String> splitQuery = List.of(trimQuery.split(" "));

        task.addData(TaskDataMapKeys.TABLE_NAME, splitQuery.get(2).split("\\(")[0].trim());

        switch (splitQuery.get(0).trim()) {
            case "create" -> {
                task.setTaskName(TaskNames.CREATE_TABLE);

                Map<String, String> columnNameByType = new LinkedHashMap<>();

                String sqlColumns = StringUtils.substringBetween(trimQuery, "(", ")").trim();
                for (String sqlColumn : sqlColumns.split(",")) {
                    if (sqlColumn.trim().split(" ").length != 2) {
                        throw new QueryException();
                    }

                    String[] columnAndType = sqlColumn.trim().split(" ");
                    columnNameByType.put(columnAndType[0].trim(), columnAndType[1].trim());
                }

                task.addData(TaskDataMapKeys.COLUMNS, columnNameByType);
            }
            case "alter" -> {
                if (splitQuery.size() < 6) {
                    throw new QueryException();
                }

                switch (splitQuery.get(3).trim()) {
                    case "rename" -> {
                        if (splitQuery.size() != 6 && !splitQuery.get(4).trim().equals("to")) {
                            throw new QueryException();
                        }

                        task.setTaskName(TaskNames.RENAME_TABLE);
                        task.addData(TaskDataMapKeys.NEW_TABLE_NAME, splitQuery.get(5).trim());
                    }
                    case "add" -> {
                        if (splitQuery.get(4).trim().equals("column")) {
                            task.setTaskName(TaskNames.ADD_COLUMNS);

                            Map<String, String> columnByTypeMap = new HashMap<>();

                            String[] splitAddColumn = trimQuery.replaceAll(",", "").split(" add column ");
                            for (int i = 1; i < splitAddColumn.length; i++) {
                                String[] splitColumnAndType = splitAddColumn[i].trim().split(" ");
                                if (splitColumnAndType.length == 2) {
                                    columnByTypeMap.put(splitColumnAndType[0].trim(), splitColumnAndType[1].trim());
                                } else {
                                    throw new QueryException();
                                }
                            }

                            task.addData(TaskDataMapKeys.COLUMNS_AND_TYPES, columnByTypeMap);
                        }
                    }
                    case "drop" -> {
                        if (splitQuery.get(4).trim().equals("column")) {
                            task.setTaskName(TaskNames.DROP_COLUMNS);
                            String[] splitDropColumn = StringUtils.stripAll(StringUtils.substringAfter(trimQuery, "drop column").replaceAll(",", "").split(" drop column "));
                            task.addData(TaskDataMapKeys.COLUMNS, splitDropColumn);
                        }
                    }
                    case "change" -> {
                        if (splitQuery.size() >= 7) {
                            task.setTaskName(TaskNames.CHANGE_COLUMNS);

                            Map<String, Map<String, String>> columnByColumnsAndTypes = new HashMap<>();

                            String[] splitChangeColumn = trimQuery.replaceAll(",", "").split(" change ");
                            for (int i = 1; i < splitChangeColumn.length; i++) {
                                Map<String, String> columnsAndTypes = new HashMap<>();

                                String[] splitColumnAndType = splitChangeColumn[i].trim().split(" ");
                                if (splitColumnAndType.length == 3) {
                                    columnsAndTypes.put(splitColumnAndType[1].trim(), splitColumnAndType[2].trim());
                                } else {
                                    throw new QueryException();
                                }

                                columnByColumnsAndTypes.put(splitColumnAndType[0].trim(), columnsAndTypes);
                            }
                            
                            task.addData(TaskDataMapKeys.COLUMN_BY_COLUMNS_AND_TYPES, columnByColumnsAndTypes);
                        }
                    }
                    default -> throw new QueryException();
                }
            }
            case "drop" -> task.setTaskName(TaskNames.DROP_TABLE);
        }
    }

    /**
     * Ex :
     * -> INSERT INTO tableName (columnName, columnName, columnName...) VALUES (value, value, value...); ✔️
     * -> INSERT INTO tableName (columnName, columnName, columnName...) VALUES (value, value, value...), (value, value, value...), (value, value, value...); ✔️
     * -> SELECT * FROM tableName; ✔️
     * -> SELECT columnName FROM tableName; ✔️
     * -> SELECT columnName, columnName, columnName... FROM tableName; ✔️
     * -> SELECT columnName, * FROM tableName; ✔️
     * -> SELECT *, columnName FROM tableName; ✔️
     * -> SELECT COUNT(*) FROM tableName; ✔️
     * -> SELECT COUNT(columnName) FROM tableName; ✔️
     * -> SELECT COUNT(DISTINCT columnName) FROM tableName; ✔️
     * -> SELECT MIN(columnName) FROM tableName; ✔️
     * -> SELECT MAX(columnName) FROM tableName; ✔️
     * -> SELECT SUM(columnName) FROM tableName; ✔️
     * -> SELECT AVG(columnName) FROM tableName; ✔️
     * -> SELECT COUNT(*), COUNT(columnName), COUNT(DISTINCT columnName), MIN(columnName), MAX(columnName), SUM(columnName), AVG(columnName) FROM tableName; ✔️
     * -> SELECT AVG(columnName) AS resultAvg FROM tableName; ✔️
     * -> SELECT columnName AS resultColumnName FROM tableName; ✔️
     * -> SELECT AVG(columnName) FROM tableName WHERE columnName = value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName = value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName != value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName < value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName > value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName <= value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName >= value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName >= value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName LIKE '%value'; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName LIKE 'value%'; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName LIKE '%value%'; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName = value AND columnName = value ...; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName = value OR columnName = value; ✔️
     * -> SELECT columnName FROM tableName WHERE columnName = value OR columnName = value OR columnName = value...; ✔️
     * -> SELECT columnName FROM tableName GROUP BY columnName; ✔️
     * -> SELECT columnName, COUNT(*) FROM tableName GROUP BY columnName; ✔️
     * -> SELECT columnName FROM tableName GROUP BY columnName, columnName; ✔️
     * -> SELECT SUM(columnName) FROM tableName GROUP BY columnName HAVING AVG(columnName) > value; ✔
     * -> SELECT AVG(columnName), columnName FROM tableName GROUP BY columnName HAVING columnName = value; ✔
     * -> SELECT columnName FROM tableName ORDER BY columnName; ✔️
     * -> SELECT columnName FROM tableName ORDER BY columnName, columnName; ✔️
     * -> SELECT columnName FROM tableName ORDER BY columnName ASC; ✔️
     * -> SELECT columnName FROM tableName ORDER BY columnName DESC; ✔️
     * -> SELECT columnName FROM tableName ORDER BY columnName ASC, columnName DESC; ✔️
     * -> SELECT columnName FROM tableName LIMIT number; ✔️
     * -> UPDATE tableName SET columnName = newValue; ✔️
     * -> UPDATE tableName SET columnName = newValue, columnName = newValue, columnName = newValue...; ✔️
     * -> UPDATE tableName SET columnName = newValue WHERE columnName = value; ✔️
     * -> UPDATE tableName SET columnName = newValue, columnName = newValue WHERE columnName = value AND columnName = value...; ✔️
     * -> UPDATE tableName SET columnName = newValue WHERE columnName = value AND columnName = value OR columnName = value...; ✔️
     * -> DELETE FROM tableName; ✔️
     * -> DELETE FROM tableName WHERE columnName = value; ✔️
     * -> DELETE FROM tableName WHERE columnName = value AND columnName = value...; ✔️
     * -> DELETE FROM tableName WHERE columnName = value AND columnName = value; OR columnName = value...; ✔️
     */
    private void setTableDataTask(String schemaName, String trimQuery, Task task) throws QueryException, SchemaException, TableException {
        List<String> splitQuery = List.of(trimQuery.split(" "));

        switch (splitQuery.get(0).trim()) {
            case "insert" -> {
                if (!splitQuery.get(1).trim().equals("into")) {
                    throw new QueryException();
                }

                task.addData(TaskDataMapKeys.TABLE_NAME, splitQuery.get(2).trim());
                task.setTaskName(TaskNames.INSERT_DATA);

                Matcher columnsValuesMatcher = Pattern.compile("\\((.*?)\\)").matcher(trimQuery);

                List<String> columnNamesSplit = null;

                List<Map<String, String>> columnsValues = new ArrayList<>();
                while (columnsValuesMatcher.find()) {
                    String content = columnsValuesMatcher.group(1);

                    if (columnNamesSplit == null) {
                        columnNamesSplit = List.of(StringUtils.stripAll(content.split(",")));
                        task.addData(TaskDataMapKeys.COLUMNS, columnNamesSplit);
                    } else {
                        if (content.split(",").length != columnNamesSplit.size()) {
                            throw new QueryException();
                        }
                        Map<String, String> row = new HashMap<>();
                        for (int i = 0; i < columnNamesSplit.size(); i++) {
                            row.put(columnNamesSplit.get(i), StringUtils.stripAll(content.split(","))[i]);
                        }
                        columnsValues.add(row);
                    }
                }

                if (columnNamesSplit == null || columnsValues.size() == 0) {
                    throw new QueryException();
                }

                task.addData(TaskDataMapKeys.COLUMNS_VALUES, columnsValues);
            }
            case "select" -> {
                if (splitQuery.size() <= 3) {
                    throw new QueryException();
                }

                task.setTaskName(TaskNames.SELECT_DATA);

                String strBetweenSelectAndFrom = StringUtils.substringBetween(trimQuery, "select", "from").trim();
                String tableName = StringUtils.substringAfter(trimQuery, "from").trim().split(" ")[0].trim();
                task.addData(TaskDataMapKeys.TABLE_NAME, tableName);

                Set<String> columns = new HashSet<>();
                Map<String, String> aggColumns = new HashMap<>();
                Set<String> columnsAndAgg = new LinkedHashSet<>();
                Map<String, String> aliases = new HashMap<>();

                List<String> selectedColumnsOrAggregations = List.of(strBetweenSelectAndFrom.split(","));
                for (String selectedColumnOrAgg : selectedColumnsOrAggregations) {
                    selectedColumnOrAgg = selectedColumnOrAgg.trim();

                    if (selectedColumnOrAgg.split(" ").length == 1 || selectedColumnOrAgg.split(" ").length == 2) {
                        String columnOrAgg = selectedColumnOrAgg.trim();

                        if (selectedColumnOrAgg.equals("*")) {
                            columns.addAll(MemoryDB.getColumns(schemaName, tableName).keySet());
                            columnsAndAgg.addAll(columns);
                        } else if (selectedColumnOrAgg.contains(("("))){
                            aggColumns.put(columnOrAgg, columnOrAgg);
                            columnsAndAgg.remove(columnOrAgg);
                            columnsAndAgg.add(columnOrAgg);
                        } else {
                            columns.add(columnOrAgg);
                            columnsAndAgg.remove(columnOrAgg);
                            columnsAndAgg.add(columnOrAgg);
                        }
                    } else {
                        String columnOrAgg = StringUtils.substringBefore(selectedColumnOrAgg, " as ").trim();
                        String alias = StringUtils.substringAfter(selectedColumnOrAgg, " as ").trim();

                        if (columnOrAgg.contains(("("))){
                            aggColumns.put(columnOrAgg, columnOrAgg);
                        } else {
                            columns.add(columnOrAgg);
                        }

                        columnsAndAgg.add(columnOrAgg);
                        aliases.put(columnOrAgg, alias);
                    }
                }

                task.addData(TaskDataMapKeys.ALIASES, aliases);
                task.addData(TaskDataMapKeys.COLUMNS, new ArrayList<>(columns));
                task.addData(TaskDataMapKeys.AGGREGATES, aggColumns);
                task.addData(TaskDataMapKeys.COLUMNS_AND_AGGREGATES, new ArrayList<>(columnsAndAgg));

                if (trimQuery.contains("where")) {
                    String[] sqlAfterWhereKeywords = {"where", "group by", "having", "order by", "limit"};

                    String whereAndOrConditions = StringUtils.substringAfter(trimQuery, "where").trim();
                    if (!whereAndOrConditions.equals("")) {
                        for (String keyword : sqlAfterWhereKeywords) {
                            whereAndOrConditions = whereAndOrConditions.split(keyword)[0].trim();
                        }
                    }

                    task.addData(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS, this.getSplitWhereAndOrConditions(whereAndOrConditions));
                }

                if (trimQuery.contains("group by")) {
                    String afterGroupBy = StringUtils.substringAfter(trimQuery, "group by").trim();
                    List<String> groupByColumns = List.of(afterGroupBy.split("order by|limit|having")[0].trim().replaceAll(" ", "").split(","));
                    task.addData(TaskDataMapKeys.GROUP_BY, groupByColumns);
                }

                if (trimQuery.contains("having")) {
                    String[] sqlAfterHavingKeywords = {"having", "order by", "limit"};
                    String havingConditions = StringUtils.substringAfter(trimQuery, "having").trim();
                    if (!havingConditions.equals("")) {
                        for (String keyword : sqlAfterHavingKeywords) {
                            havingConditions = havingConditions.split(keyword)[0].trim();
                        }
                    }
                    task.addData(TaskDataMapKeys.HAVING, this.getSplitWhereAndOrConditions(havingConditions));
                }

                if (trimQuery.contains("order by")) {
                    String afterOrderBy = StringUtils.substringAfter(trimQuery, "order by").trim();

                    String[] orderByColumns = StringUtils.stripAll(afterOrderBy.split(","));
                    Collections.reverse(Arrays.asList(orderByColumns));
                    Map<String, String> orderByColumnsMap = new LinkedHashMap<>();

                    for (String columnAndOrder : orderByColumns) {
                        String[] columnByOrder = StringUtils.stripAll(columnAndOrder.split(" "));

                        if (columnByOrder.length > 2) {
                            throw new QueryException();
                        }

                        if (columnByOrder.length == 1) {
                            orderByColumnsMap.put(columnByOrder[0].trim(), "asc");
                        } else {
                            String order = columnByOrder[1].trim();
                            if (order.equals("asc") || order.equals("desc")) {
                                orderByColumnsMap.put(columnByOrder[0].trim(), columnByOrder[1].trim());
                            } else {
                                throw new QueryException();
                            }
                        }
                    }

                    task.addData(TaskDataMapKeys.ORDER_BY, orderByColumnsMap);
                }

                if (trimQuery.contains("limit"))
                    task.addData(TaskDataMapKeys.LIMIT, StringUtils.substringAfter(trimQuery, "limit").trim().split(" ")[0]);
            }
            case "update" -> {
                task.setTaskName(TaskNames.UPDATE_DATA);
                task.addData(TaskDataMapKeys.TABLE_NAME, splitQuery.get(1).trim());

                if (!splitQuery.get(2).trim().equals("set")) {
                    throw new QueryException();
                }

                Map<String, String> columnByNewValueMap = new HashMap<>();

                String[] columnsByNewValues = StringUtils.substringAfter(trimQuery, "set").trim().split("where")[0].trim().replaceAll(" ", "").split(",");
                for (String columnByNewValue : columnsByNewValues) {
                    String[] cv = columnByNewValue.split("=");
                    columnByNewValueMap.put(cv[0], cv[1]);
                }

                task.addData(TaskDataMapKeys.COLUMNS_AND_NEW_VALUES, columnByNewValueMap);

                if (trimQuery.contains("where"))
                    task.addData(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS, this.getSplitWhereAndOrConditions(StringUtils.substringAfter(trimQuery, "where").trim()));
            }
            case "delete" -> {
                task.setTaskName(TaskNames.DELETE_DATA);
                task.addData(TaskDataMapKeys.TABLE_NAME, splitQuery.get(2).trim());

                if (splitQuery.size() == 3) {
                    task.addData(TaskDataMapKeys.DELETE_ALL_DATA, "*");
                }

                if (splitQuery.size() > 6 && splitQuery.get(3).trim().equals("where"))
                    task.addData(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS, this.getSplitWhereAndOrConditions(StringUtils.substringAfter(trimQuery, "where").trim()));
            }
        }
    }

    private List<String> getSplitWhereAndOrConditions(String whereAndOrConditions) {
        List<String> conditions = new ArrayList<>();

        List<String> operatorKeywords = new LinkedList<>(List.of( "!=", "<=", ">=", "<", ">", "=", " like ", " and ", " or "));
        StringBuilder str = new StringBuilder();
        char[] conditionsChar = whereAndOrConditions.toCharArray();
        for (int i = 0; i < conditionsChar.length; i++) {
            str.append(conditionsChar[i]);

            for (String keyword : operatorKeywords) {
                if (str.toString().contains(keyword)) {
                    if ((i + 1) < conditionsChar.length) {
                        String keyword1 = (keyword + conditionsChar[i + 1]).trim();
                        if (operatorKeywords.contains(keyword1))
                            break;
                    }

                    conditions.add(str.toString().replace(keyword, "").trim());
                    conditions.add(keyword.trim());
                    str.setLength(0);

                    break;
                }
            }
        }

        conditions.add(str.toString().trim());

        return conditions;
    }

    public static void main(String[] args) throws QueryException {
        SQLQueryService sqlQueryService = new SQLQueryService();
        Task task = new Task();
        sqlQueryService.setTableTask("ALTER TABLE table1 ADD COLUMN columnName float", task);
        System.out.println(task);
    }

}
