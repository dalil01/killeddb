package com.alphasquad.killeddb.factories;

import com.alphasquad.killeddb.core.storage.data.columns.*;
import com.alphasquad.killeddb.enums.ColumnTypes;

public class ColumnFactory {

    private static Column instance = null;

    public static Column createColumn(String name, ColumnTypes type) {
        switch (type) {
            case BOOLEAN -> ColumnFactory.instance = new ColumnBoolean(name, type);
            case BYTE -> ColumnFactory.instance = new ColumnByte(name, type);
            case CHARACTER -> ColumnFactory.instance = new ColumnCharacter(name, type);
            case DATETIME -> ColumnFactory.instance = new ColumnDatetime(name, type);
            case DOUBLE -> ColumnFactory.instance = new ColumnDouble(name, type);
            case FLOAT -> ColumnFactory.instance = new ColumnFloat(name, type);
            case INTEGER -> ColumnFactory.instance = new ColumnInteger(name, type);
            case LONG -> ColumnFactory.instance = new ColumnLong(name, type);
            case SHORT -> ColumnFactory.instance = new ColumnShort(name, type);
            case STRING -> ColumnFactory.instance = new ColumnString(name, type);
        }

        return ColumnFactory.instance;
    }

    public static Column get() {
        return ColumnFactory.instance;
    }

}
