package com.alphasquad.killeddb.utils;

import java.util.ArrayList;
import java.util.List;

public class Splitter {

    public static List<String> split(String str, char delimiter) {
        List<String> tokensList = new ArrayList<>();

        StringBuilder builder = new StringBuilder();

        boolean inQuotes = false;
        for (char c : str.toCharArray()) {
            if (c == delimiter) {
                if (inQuotes) {
                    builder.append(c);
                } else {
                    tokensList.add(builder.toString());
                    builder = new StringBuilder();
                }
            } else if (c == '\"') {
                inQuotes = !inQuotes;
            } else {
                builder.append(c);
            }
        }

        tokensList.add(builder.toString());

        return tokensList;
    }

}
