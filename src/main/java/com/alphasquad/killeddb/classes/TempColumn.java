package com.alphasquad.killeddb.classes;

import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.booleans.BooleanArrayList;
import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.chars.CharArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;

import java.util.*;

public class TempColumn {

    private final Column<Class<?>> column;
    private final ColumnTypes columnTypes;

    private Collection<?> values;

    private BooleanArrayList boolValues;
    private ByteArrayList byteValues;
    private CharArrayList charValues;
    private LongArrayList datetimeValues;
    private DoubleArrayList doubleValues;
    private FloatArrayList floatValues;
    private IntArrayList intValues;
    private LongArrayList longValues;
    private ShortArrayList shortValues;
    private ArrayList<String> stringValues;

    public TempColumn(Column<Class<?>> column) {
        this.column = column;
        this.columnTypes = this.column.getType();
        this.initValues();
    }

    private void initValues() {
        switch (this.columnTypes) {
            case BOOLEAN -> {
                this.boolValues = new BooleanArrayList();
                this.values = this.boolValues;
            }
            case BYTE -> {
                this.byteValues = new ByteArrayList();
                this.values = this.byteValues;
            }
            case CHARACTER -> {
                this.charValues = new CharArrayList();
                this.values = this.charValues;
            }
            case DATETIME -> {
                this.datetimeValues = new LongArrayList();
                this.values = this.datetimeValues;
            }
            case DOUBLE -> {
                this.doubleValues = new DoubleArrayList();
                this.values = this.doubleValues;
            }
            case FLOAT -> {
                this.floatValues = new FloatArrayList();
                this.values = this.floatValues;
            }
            case INTEGER -> {
                this.intValues = new IntArrayList();
                this.values = this.intValues;
            }
            case LONG -> {
                this.longValues = new LongArrayList();
                this.values = this.longValues;
            }
            case SHORT -> {
                this.shortValues = new ShortArrayList();
                this.values = this.shortValues;
            }
            default -> {
                this.stringValues = new ArrayList<>();
                this.values = this.stringValues;
            }
        }
    }

    public void addValue(String value) {
        switch (this.columnTypes) {
            case BOOLEAN -> this.boolValues.add(Parser.parseBoolean(value));
            case BYTE -> this.byteValues.add(Parser.parseByte(value));
            case CHARACTER -> this.charValues.add(Parser.parseChar(value));
            case DATETIME -> this.datetimeValues.add(Parser.parseDate(value));
            case DOUBLE -> this.doubleValues.add(Parser.parseDouble(value));
            case FLOAT -> this.floatValues.add(Parser.parseFloat(value));
            case INTEGER -> this.intValues.add(Parser.parseInt(value));
            case LONG -> this.longValues.add(Parser.parseLong(value));
            case SHORT -> this.shortValues.add(Parser.parseShort(value));
            default -> this.stringValues.add(value);
        }
    }

    public void merge() {
        this.column.addValues(this.values);
    }

}
