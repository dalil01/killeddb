package com.alphasquad.killeddb.exceptions;

public class QueryException extends Exception {

    public QueryException() {
        super("Invalid query.");
    }

    public QueryException(String message) {
        super(message);
    }

}
