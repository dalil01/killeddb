package com.alphasquad.killeddb.exceptions;

public class TableException extends Exception{

    public TableException(String message) {
        super(message);
    }

}
