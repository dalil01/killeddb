package com.alphasquad.killeddb.exceptions.mappers;

import com.alphasquad.killeddb.exceptions.QueryException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class TableExceptionMapper implements ExceptionMapper<QueryException> {

    @Override
    public Response toResponse(QueryException e) {
        return Response.status(400).entity(e.getMessage()).type("plain/text").build();
    }

}
