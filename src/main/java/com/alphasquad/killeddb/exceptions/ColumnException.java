package com.alphasquad.killeddb.exceptions;

public class ColumnException extends Exception {

    public ColumnException(String message) {
        super(message);
    }

}
