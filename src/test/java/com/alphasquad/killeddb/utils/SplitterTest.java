package com.alphasquad.killeddb.utils;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SplitterTest {

    @Test
    public void testSplit() {
        String str = "nikolagjorgjievski,34368385,251381442,Do you think its better if we use cards here instead of a list ?\\https://getbootstrap.com/docs/4.0/components/card/,journal-app,,TamaraStankovska,13293313,54298419,1282815601,2019-01-07 17:11:19 UTC\n";
        List<String> strSplit = Splitter.split(str, ',');
        assertEquals(11, strSplit.size());
        assertEquals("nikolagjorgjievski", strSplit.get(0));
        assertEquals("251381442", strSplit.get(2));
    }

}
