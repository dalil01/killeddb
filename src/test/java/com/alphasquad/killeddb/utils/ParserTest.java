package com.alphasquad.killeddb.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParserTest {

    @Test
    public void testParseBoolean() {
        assertTrue(Parser.parseBoolean("true"));
        assertTrue(Parser.parseBoolean("yes"));
        assertTrue(Parser.parseBoolean("1"));

        assertFalse(Parser.parseBoolean("0"));
        assertFalse(Parser.parseBoolean("blabla"));
        assertFalse(Parser.parseBoolean(""));
    }

    @Test
    public void testParseByte() {
        assertEquals(0, (byte) Parser.parseByte(""));
        assertEquals(1, (byte) Parser.parseByte("1"));
        assertEquals(100, (byte) Parser.parseByte("100"));
    }

    @Test
    public void testParseChar() {
        assertEquals(Parser.parseChar(""), Character.MIN_VALUE);
        assertEquals(Parser.parseChar("N"), 'N');
        assertEquals(Parser.parseChar("ABC"), 'A');
    }

    @Test
    public void testParseDate() {
        assertEquals(Parser.parseDate(""), 0);
        assertEquals(Parser.parseDate("1970-01-01 12:00:00"), -3600000);
        assertEquals(Parser.parseDate("2015-12-01 00:05:16"), 1448924716000L);
    }

    @Test
    public void testParseDateFormat() {
        assertNull(Parser.parseDate(0));
        assertEquals(Parser.parseDate(-3600000), "1970-01-01 12:00:00");
        assertEquals(Parser.parseDate(1448924716000L), "2015-12-01 12:05:16");
    }

    @Test
    public void testParseDouble() {
        assertEquals(0.0, Parser.parseDouble(""), 0.0);
        assertEquals(0.0, Parser.parseDouble("0"), 0.0);
        assertEquals(80.5, Parser.parseDouble("80.5"), 0.0);
        assertEquals(50.99, Parser.parseDouble("50.99"), 0.0);
        assertEquals(100.00, Parser.parseDouble("100"), 0.0);
        assertEquals(3.00, Parser.parseDouble("3."), 0.0);
    }

    @Test
    public void testParseFloat() {
        assertEquals(0.0, Parser.parseFloat(""), 0.0);
        assertEquals(0.0, Parser.parseFloat("0"), 0.0);
        assertEquals(80.5, Parser.parseFloat("80.5"), 0.0);
        assertEquals(50.9900016784668, Parser.parseFloat("50.99"), 0.0);
        assertEquals(100.00, Parser.parseFloat("100"), 0.0);
        assertEquals(3.00, Parser.parseFloat("3."), 0.0);
    }

    @Test
    public void testParseInt() {
        assertEquals(Parser.parseInt(""), 0);
        assertEquals(Parser.parseInt("10"), 10);
        assertEquals(Parser.parseInt("3"), 3);
    }

    @Test
    public void testParseLong() {
        assertEquals(Parser.parseLong(""), 0);
        assertEquals(Parser.parseLong("1550"), 1550);
        assertEquals(Parser.parseLong("3848484"), 3848484);
    }

    @Test
    public void testParseShort() {
        assertEquals(Parser.parseShort(""), 0);
        assertEquals(Parser.parseShort("150"), 150);
        assertEquals(Parser.parseShort("3884"), 3884);
    }

}
