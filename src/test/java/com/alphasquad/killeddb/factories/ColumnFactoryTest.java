package com.alphasquad.killeddb.factories;

import com.alphasquad.killeddb.core.storage.data.columns.*;
import com.alphasquad.killeddb.enums.ColumnTypes;
import org.junit.Test;

import static org.junit.Assert.*;

public class ColumnFactoryTest {

    private static final String columnName = "column";

    @Test
    public void testCreateColumnBoolean() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.BOOLEAN);
        assertTrue(column instanceof ColumnBoolean);
    }

    @Test
    public void testCreateColumnByte() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.BYTE);
        assertTrue(column instanceof ColumnByte);
    }

    @Test
    public void testCreateColumnCharacter() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.CHARACTER);
        assertTrue(column instanceof ColumnCharacter);
    }

    @Test
    public void testCreateColumnDatetime() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.DATETIME);
        assertTrue(column instanceof ColumnDatetime);
    }

    @Test
    public void testCreateColumnDouble() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.DOUBLE);
        assertTrue(column instanceof ColumnDouble);
    }

    @Test
    public void testCreateColumnFloat() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.FLOAT);
        assertTrue(column instanceof ColumnFloat);
    }

    @Test
    public void testCreateColumnInteger() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.INTEGER);
        assertTrue(column instanceof ColumnInteger);
    }

    @Test
    public void testCreateColumnLong() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.LONG);
        assertTrue(column instanceof ColumnLong);
    }

    @Test
    public void testCreateColumnShort() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.SHORT);
        assertTrue(column instanceof ColumnShort);
    }

    @Test
    public void testCreateColumnString() {
        Column column = ColumnFactory.createColumn(columnName, ColumnTypes.STRING);
        assertTrue(column instanceof ColumnString);
    }

}
