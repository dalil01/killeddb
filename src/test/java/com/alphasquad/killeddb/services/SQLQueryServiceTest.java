package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.QueryException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import com.alphasquad.killeddb.factories.ColumnFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class SQLQueryServiceTest {

    private final SQLQueryService sqlQueryService = new SQLQueryService();

    private static final String schemaName = "schema";
    private static final String tableName = "table";
    private static final List<String> columnNames = List.of("column1", "column2", "column3");

    @BeforeClass
    public static void setUp() throws SchemaException, TableException, ColumnException {
        MemoryDB.createSchema(new Schema(schemaName));
        MemoryDB.createTable(schemaName, tableName);
        for (String columnName : columnNames) {
            MemoryDB.addColumn(schemaName, tableName, ColumnFactory.createColumn(columnName, ColumnTypes.STRING));
        }
    }

    @Test
    public void testGetCreateSchemaTask() throws SchemaException, QueryException, TableException {
        String req = "CREATE SCHEMA " + schemaName;

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.CREATE_SCHEMA);
        assertEquals(dataMap.get(TaskDataMapKeys.SCHEMA_NAME), schemaName);
    }

    @Test
    public void testGetRenameSchemaTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER SCHEMA " + schemaName + " RENAME newSchema;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.RENAME_SCHEMA);
        assertEquals(dataMap.get(TaskDataMapKeys.SCHEMA_NAME), schemaName);
        assertEquals(dataMap.get(TaskDataMapKeys.NEW_SCHEMA_NAME), "newschema");
    }

    @Test
    public void testGetDropSchemaTask() throws SchemaException, QueryException, TableException {
        String req = "DROP SCHEMA " + schemaName;

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.DROP_SCHEMA);
        assertEquals(dataMap.get(TaskDataMapKeys.SCHEMA_NAME), schemaName);
    }

    @Test
    public void testGetCreateTableTask() throws SchemaException, QueryException, TableException {
        String req = "CREATE TABLE " + tableName + " (column1 string, column2 int, column3 boolean);";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.CREATE_TABLE);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);
        assertNotNull(dataMap.get(TaskDataMapKeys.COLUMNS));

        Map<String, String> columnNameByType = (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(columnNameByType.get("column1"), "string");
        assertEquals(columnNameByType.get("column2"), "int");
        assertEquals(columnNameByType.get("column3"), "boolean");
    }

    @Test
    public void testGetRenameTableTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " RENAME TO newTable;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.RENAME_TABLE);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);
        assertEquals(dataMap.get(TaskDataMapKeys.NEW_TABLE_NAME), "newtable");
    }

    @Test
    public void testGetAddColumnTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " ADD COLUMN columnString string;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.ADD_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        Map<String, String> columnsAndTypes = (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_TYPES);
        assertEquals(1, columnsAndTypes.size());
        assertEquals(columnsAndTypes.get("columnstring"), "string");
    }

    @Test
    public void testGetAddColumnsTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " ADD COLUMN columnString string, ADD COLUMN columnBool boolean, ADD COLUMN columnDate datetime;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.ADD_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        Map<String, String> columnsAndTypes = (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_TYPES);
        assertEquals(3, columnsAndTypes.size());
        assertEquals(columnsAndTypes.get("columnstring"), "string");
        assertEquals(columnsAndTypes.get("columnbool"), "boolean");
        assertEquals(columnsAndTypes.get("columndate"), "datetime");
    }

    @Test
    public void testGetChangeColumnTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " CHANGE columnString newColumnString string;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.CHANGE_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        Map<String, Map<String, String>> columnsAndTypes = (Map<String, Map<String, String>>) dataMap.get(TaskDataMapKeys.COLUMN_BY_COLUMNS_AND_TYPES);
        assertEquals(1, columnsAndTypes.size());
        Map<String, String> columnStringByNewColumn = columnsAndTypes.get("columnstring");
        assertEquals(columnStringByNewColumn.get("newcolumnstring"), "string");
    }

    @Test
    public void testGetChangeColumnsTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " CHANGE columnString newColumnString string, CHANGE columnLong newColumnLong long;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.CHANGE_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        Map<String, Map<String, String>> columnsAndTypes = (Map<String, Map<String, String>>) dataMap.get(TaskDataMapKeys.COLUMN_BY_COLUMNS_AND_TYPES);
        assertEquals(2, columnsAndTypes.size());

        Map<String, String> columnStringByNewColumnString = columnsAndTypes.get("columnstring");
        assertEquals(columnStringByNewColumnString.get("newcolumnstring"), "string");

        Map<String, String> columnStringByNewColumnLong = columnsAndTypes.get("columnlong");
        assertEquals(columnStringByNewColumnLong.get("newcolumnlong"), "long");
    }

    @Test
    public void testGetDropColumnTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " DROP COLUMN column;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.DROP_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        String[] dropColumns = (String[]) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(1, dropColumns.length);
        assertEquals(dropColumns[0], "column");
    }

    @Test
    public void testGetDropColumnsTask() throws SchemaException, QueryException, TableException {
        String req = "ALTER TABLE " + tableName + " DROP COLUMN column1, DROP COLUMN column2, DROP COLUMN column3;";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.DROP_COLUMNS);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        String[] dropColumns = (String[]) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(3, dropColumns.length);
        assertEquals(dropColumns[0], "column1");
        assertEquals(dropColumns[1], "column2");
        assertEquals(dropColumns[2], "column3");
    }

    @Test
    public void testGetDropTableTask() throws SchemaException, QueryException, TableException {
        String req = "DROP TABLE " + tableName;

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.DROP_TABLE);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);
    }

    @Test
    public void testGetInsertDataTask() throws SchemaException, QueryException, TableException {
        String req = "INSERT INTO " + tableName + " (column1, column2, column3) VALUES (value1, value2, value3)";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.INSERT_DATA);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        List<String> columns = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(3, columns.size());
        assertEquals(columns.get(0), "column1");
        assertEquals(columns.get(1), "column2");
        assertEquals(columns.get(2), "column3");

        List<Map<String, String>> columnByValues = (List<Map<String, String>>) dataMap.get(TaskDataMapKeys.COLUMNS_VALUES);
        assertEquals(1, columnByValues.size());
        assertEquals(columnByValues.get(0).get("column1"), "value1");
        assertEquals(columnByValues.get(0).get("column2"), "value2");
        assertEquals(columnByValues.get(0).get("column3"), "value3");
    }

    @Test
    public void testGetInsertMultipleDataTask() throws SchemaException, QueryException, TableException {
        String req = "INSERT INTO " + tableName + " (column1, column2, column3) VALUES (value1, value2, value3), (value1, value2, value3)";

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        assertEquals(task.getTaskName(), TaskNames.INSERT_DATA);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        List<String> columns = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(3, columns.size());
        assertEquals(columns.get(0), "column1");
        assertEquals(columns.get(1), "column2");
        assertEquals(columns.get(2), "column3");

        List<Map<String, String>> columnByValues = (List<Map<String, String>>) dataMap.get(TaskDataMapKeys.COLUMNS_VALUES);
        assertEquals(2, columnByValues.size());
        assertEquals(columnByValues.get(0).get("column1"), "value1");
        assertEquals(columnByValues.get(0).get("column2"), "value2");
        assertEquals(columnByValues.get(0).get("column3"), "value3");
        assertEquals(columnByValues.get(1).get("column1"), "value1");
        assertEquals(columnByValues.get(1).get("column2"), "value2");
        assertEquals(columnByValues.get(1).get("column3"), "value3");
    }

    @Test
    public void testGetSelectAllDataTask() throws SchemaException, QueryException, TableException {
        String req = "SELECT * FROM " + tableName;

        Task task = sqlQueryService.getTask(schemaName, req);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();
        System.out.println(task);

        assertEquals(task.getTaskName(), TaskNames.SELECT_DATA);
        assertEquals(dataMap.get(TaskDataMapKeys.TABLE_NAME), tableName);

        List<String> columns = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS);
        assertEquals(3, columns.size());
        assertTrue(columns.contains("column1"));
        assertTrue(columns.contains("column2"));
        assertTrue(columns.contains("column3"));

        Map<String, String> aggregates = (Map<String, String>) dataMap.get(TaskDataMapKeys.AGGREGATES);
        assertEquals(0, aggregates.size());

        List<String> columnsAggregates = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_AGGREGATES);
        assertEquals(3, columnsAggregates.size());
        assertTrue(columnsAggregates.contains("column1"));
        assertTrue(columnsAggregates.contains("column2"));
        assertTrue(columnsAggregates.contains("column3"));

        Map<String, String> aliases = (Map<String, String>) dataMap.get(TaskDataMapKeys.ALIASES);
        assertEquals(0, aliases.size());
    }


    @Test
    public void testGetUpdateDataTask() throws SchemaException, QueryException, TableException {
        // TODO
    }

}
